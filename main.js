"use strict";

function levelNameValid(name) {
    return name.length > 3 && name.length < 256;
}

document.addEventListener("DOMContentLoaded", () => {
    let statusBar = new StatusBar();

    let editor = new LevelEditor(statusBar);
    editor.addTypes(levelTypes);
    editor.levelData.resize(5, 5);
    editor.levelView.resize(5, 5);

    let levelSizeButton = document.getElementById("setLevelSize");
    let levelSizeW = document.getElementById("levelSizeW");
    let levelSizeH = document.getElementById("levelSizeH");
    levelSizeButton.addEventListener("click", () => {
        if(levelSizeW.value >= 5 && levelSizeW.value <= 64 &&
           levelSizeH.value >= 5 && levelSizeH.value <= 64) {
            editor.levelData.resize(levelSizeW.value, levelSizeH.value);
            editor.levelView.resize(levelSizeW.value, levelSizeH.value);
        } else {
            statusBar.flashError("level size too big or too small");
        }
    });

    let levelNameInput = document.getElementById("levelName");
    let saveLevelButton = document.getElementById("saveLevel");
    let loadLevelButton = document.getElementById("loadLevel");
    //let generateBorders = document.getElementById("generateBorders");

    //generateBorders.addEventListener("click", () => {
    //    LevelEditor.setType(WallType);
    //});

    saveLevelButton.addEventListener("click", () => {
        let levelName = levelNameInput.value;
        console.log(JSON.stringify(editor.getData()));
        if(levelNameValid(levelName)) {
            fetch(`/levels/${levelName}`, { 
                method: "POST",
                body: JSON.stringify(editor.getData()),
                headers: { "Content-Type": "application/json" },
            })
                .then(r => r.json())
                .then(response => {
                    console.log(response);
                    if(response.message == "success") {
                        statusBar.flashInfo("level saved");
                    } else {
                        console.log(response);
                        statusBar.flashError("error while saving the level");
                    }
                })
                .catch(error => {
                    console.log(error);
                    statusBar.flashError("error while saving the level");
                });
        } else {
            statusBar.flashError("invalid level name");
        }
    });

    loadLevelButton.addEventListener("click", () => {
        let levelName = levelNameInput.value;
        fetch(`/levels/${levelName}`)
            .then(r => r.json())
            .then(level => {
                if(level != null) {
                    editor.loadLevel(level);
                    statusBar.flashInfo("level loaded");
                } else {
                    statusBar.flashError("no such level");
                }
            })
            .catch(error => {
                console.log(error);
                statusBar.flashError("error while loading data");
            })
    });
});
