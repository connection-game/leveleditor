"use strict";

class LevelView {
    constructor(dom=document.getElementById("levelView")) {
        this.dom = dom;
        this.originalDom = this.dom.cloneNode(true);
        this.place = false;
        this.onCellClicked = () => {};
        this.cellDom = document.createElement("div");;
        this.cellGroup = "wall";
        this.types = [];

        document.addEventListener("mousedown", () => this.place = true);
        document.addEventListener("mouseup", () => this.place = false);
        this.dom.parentNode.addEventListener("mouseleave", () => this.place = false);
    }

    generate(w, h) {
        while(this.dom.firstChild)
            this.dom.removeChild(this.dom.firstChild);

        for(let y = 0; y != h; y++) {
            let row = document.createElement("div");
            row.classList.add("row");
            this.dom.append(row);
            for(let x = 0; x != w; x++) {
                let cellContainer = document.createElement("div");
                cellContainer.classList.add("cellContainer");
                cellContainer.addEventListener("mouseenter", () => this.place ? this.setCell(x, y, cellContainer) : undefined );
                cellContainer.addEventListener("mousedown", () => this.setCell(x, y, cellContainer));
                row.append(cellContainer);
            }
        }
    }

    setCell(x, y, cellContainer) {
        let newCell = this.cellDom.cloneNode(true);
        if(this.onCellClicked(x, y, newCell)) {
            if(this.cellGroup == null) { //i be hating on others code but in reality i hate my own code
                while(cellContainer.children[0] != undefined)
                    cellContainer.removeChild(cellContainer.children[0]);
            }

            let nullCell = cellContainer.getElementsByClassName("nullGroup")[0];
            if(nullCell != undefined) 
                cellContainer.removeChild(nullCell); //send help

            let oldCell = cellContainer.getElementsByClassName(this.cellGroup + "Group")[0];
            newCell.style.zIndex = cellContainer.children.length+1;

            if(oldCell == undefined) {
                cellContainer.append(newCell);
            } else {
                cellContainer.replaceChild(newCell, oldCell);
            }
        }
    }

    resize(w, h) {
        let oldDom = this.dom.cloneNode(true);

        this.generate(w, h);

        for(let n in oldDom.children) {
            for(let m in oldDom.children[n].children) {
                if(this.dom.children.length > n && this.dom.children[0].children.length > m) {
                    this.dom.children[n].replaceChild(oldDom.children[n].children[m].cloneNode(true), 
                        this.dom.children[n].children[m]);

                    this.dom.children[n].children[m].addEventListener("mouseenter", () => 
                        this.place ? this.setCell(m, n, this.dom.children[n].children[m]) : undefined );
                    this.dom.children[n].children[m].addEventListener("mousedown", () => 
                        this.setCell(m, n, this.dom.children[n].children[m]));
                }
            }
        }
    }

    setType(type) {
        this.currentType = type;
        this.cellGroup = type.group;
        this.cellDom = type.cellDom;
    }

    addType(type) {
        this.types.push(type);
    }

    loadData(data) {
        console.table(data);
        this.generate(data[0].length, data.length);
        data.forEach((row, y) => {
            row.forEach((field, x) => {
                Object.keys(field).forEach(group => {
                    let type = this.types.find(t => (t.group == group || t.group == null) && t.matchData(field[group]));  //n^4???? 
                    if(type != undefined) {
                        let cell = type.generateCell(field[group]);
                        this.dom.children[y].children[x].append(cell.cloneNode(true));

                        this.dom.children[y].children[x].addEventListener("mouseenter", () => 
                            this.place ? this.setCell(x, y, this.dom.children[y].children[x]) : undefined );
                        this.dom.children[y].children[x].addEventListener("mousedown", () => 
                            this.setCell(x, y, this.dom.children[y].children[x]));
                    } else {
                        console.log("no type found for field", field);
                    }
                });
            });
        });
    }

    setOnCellClicked(fun) {
        this.onCellClicked = fun;
    }
}
