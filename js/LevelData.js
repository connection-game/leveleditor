"use strict";

class LevelData {
    constructor() {
        this.data = [];
        this.currentData = {};
        this.currentGroup = "";
        this.types = [];
    }

    generate(w, h, defaultData={}) {
        for(let y = 0; y != h; y++) {
            let row = [];
            for(let x = 0; x != w; x++) {
                let cell = Object.assign({}, defaultData);
                row.push(cell);
            }
            this.data.push(row);
        }
    }

    resize(w, h) {
        let oldData = this.data.slice(0);
        this.data = [];
        this.generate(w, h);

        this.data.forEach((row, y) => {
            row.forEach((cell, x) => {
                if(oldData.length > y && oldData[0].length > x) {
                    if(oldData[y][x] instanceof Object) {
                        this.data[y][x] = Object.assign({}, oldData[y][x]);
                    } else {
                        this.data[y][x] = oldData[y][x];
                    }
                }
            });
        });
    }

    setType(type) {
        this.currentType = type;
        this.currentGroup = type.group;
        if(type.data instanceof Object) {
            this.currentData = Object.assign({}, type.data);
        } else {
            this.currentData = type.data;
        }
    }

    setCell(x, y, data=this.currentData, group=this.currentGroup) {
        let constraintResult = this.currentType.constraint(this.data, x, y);
        if(constraintResult[0]) {
            if(data instanceof Object) {
                if(group !== null) {
                    this.data[y][x][group] = Object.assign({}, data); //not sure if object.asssign is needed here
                } else {
                    this.data[y][x] = Object.assign({}, data); //not sure if object.asssign is needed here
                }
            } else {
                if(group !== null) {
                    this.data[y][x][group] = data;
                } else {
                    this.data[y][x] = data;
                }
            }
        }
        return constraintResult;
    }

    addType(type) {
        this.types.push(type);
    }

    convertData() {
        return this.data.map(row => row.map(d => { 
            return this.types.map(t => t.__proto__.converter).reduce((p, curr) => curr instanceof Function ? d = curr(d) : d, 
                Object.assign({}, d)); //thats good code right there
        }));
    }

    getData() {
        return this.convertData();
    }

    deconvertData() {
        return this.data.map(row => row.map(d => { 
            return this.types.map(t => t.__proto__.deconverter).reduce((p, curr) => curr instanceof Function ? d = curr(d) : d, 
                Object.assign({}, d)); //performance much?
        }));
        //also, having a bad feeling about this. might break anytime tbh
        //this d = curr(d) fragment makes me anxious
    }

    loadData(data) {
        this.data = data;
        this.data = this.deconvertData(data); //epic code bro
    }
}
