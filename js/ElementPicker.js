"use strict";

class ElementPicker {
    constructor(dom=document.getElementById("picker")) {
        this.dom = dom;
        this.types = [];
        this.currentData = {};
        this.currentType = undefined;

        this.onChange = () => {};
    }

    addType(typeInstance) {
        typeInstance.setOnChange((data) => {
            this.setCurrentType(typeInstance);
            this.currentData = data;
            this.onChange(this.currentType);
        });

        typeInstance.typeSettingsDom.addEventListener("click", () => {
            this.setCurrentType(typeInstance);
            this.onChange(this.currentType);
        });

        this.types.push(typeInstance);
        this.dom.append(typeInstance.typeSettingsDom);
    }

    setCurrentType(typeInstance) {
        if(this.currentType != undefined) {
            this.currentType.typeSettingsDom.classList.remove("selectedType");
        }

        typeInstance.typeSettingsDom.classList.add("selectedType");
        this.currentType = typeInstance;
    }

    setOnChange(fun) {
        this.onChange = fun;
    }
}
