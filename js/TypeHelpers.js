"use strict";

let levelTypes = [];

class Type { }

function generateDefaultType(name, data, group="block") {
    let type = class extends Type {
        constructor() {
            super();
            this.data = data;
            this.group = group;
            this.initCellDom();
            this.initTypeSettingsDom();
            this.onChange = () => {};
        }

        static getGroup() {
            return group;
        }

        matchData(d) {
            return JSON.stringify(d) == JSON.stringify(data); //not recommended
        }

        constraint(levelData, x, y) {
            if(levelData[y][x].wall !== null) {
                return [true];
            } else {
                return [false, "block cannot be placed on an empty field"];
            }
        }

        levelDataConverter(data) {
            if(data.wall == null) {
                return null;
            }
        }

        initCellDom() {
            this.cellDom = document.createElement("div");
            this.cellDom.classList.add("cell");
            this.cellDom.classList.add(name + "Type");
            this.cellDom.classList.add(group + "Group");
            if(this.customInitCellDom instanceof Function) {
                this.customInitCellDom();
            }
        }

        initTypeSettingsDom() {
            this.typeSettingsDom = document.createElement("div");
            this.typeSettingsDom.classList.add("typeSettings");

            let title = document.createElement("h3");
            title.innerText = name;
            title.classList.add("title");
            this.typeSettingsDom.append(title);
        }

        generateCell(data) {
            return this.cellDom;
        }

        setOnChange(fun) {
            this.onChange = fun;
        }
    }

    levelTypes.push(type);
    return type;
}

function generateTextDiv(text) {
    return function() {
        let intDiv = document.createElement("div");
        intDiv.textContent = text;
        intDiv.classList.add("textDiv");
        this.cellDom.append(intDiv);
        this.cellDom.classList.add("textDivContainer");
    };
}

function singletonConstraint(name) {
    return (data) => {
        let count = data.reduce((p, v) => 
            p += v.reduce((p, v) => 
                p += v != undefined && v.block != undefined && v.wall !== null && v.block.block == name ? 1 : 0, 
            0), 
        0);

        if(count >= 1) {
            return [false, "only one instance of this object can be placed"];
        } else {
            return [true];
        }
    }
}
