"use strict";

let DoorType = generateDefaultType("door", { "block": "door" });
DoorType.prototype.customInitCellDom = generateTextDiv("D");
DoorType.prototype.matchData = (d) => d.block == "door";
DoorType.prototype.constraint = function () {
    return [true];
}

DoorType.prototype.initTypeSettingsDom = function() {
    this.typeSettingsDom = document.createElement("div");
    this.typeSettingsDom.classList.add("typeSettings");

    let title = document.createElement("h3");
    title.innerText = "door";
    title.classList.add("title");
    this.typeSettingsDom.append(title);

    let inputContainer = document.createElement("div");
    inputContainer.classList.add("objectIdContainer");

    let label = document.createElement("span");
    label.innerHTML = "door id";
    label.classList.add("objectIdContainer");

    let input = document.createElement("input");
    input.maxLength = 4;

    let statusBar = new StatusBar()
    let saveButton = document.createElement("button");
    saveButton.innerHTML = "save";
    saveButton.addEventListener("click", () => {
        if(input.value.includes("\"")) {
            statusBar.flashError("id contains invalid characters");
        } else {
            this.data.id = input.value;
            this.doorIdDom.textContent = input.value;
        }
    });

    inputContainer.append(label, input, saveButton);
    this.typeSettingsDom.append(inputContainer);
};

DoorType.prototype.customInitCellDom = function() {
    let intDiv = document.createElement("div");
    intDiv.textContent = "D";
    intDiv.classList.add("textDiv");

    this.doorIdDom = document.createElement("span");
    this.doorIdDom.classList.add("objectId");

    this.cellDom.append(intDiv, this.doorIdDom);
    this.cellDom.classList.add("textDivContainer");
}

DoorType.prototype.generateCell = function (data) {
    let node = this.cellDom.cloneNode(true);
    node.querySelector(".objectId").textContent = data.id;
    return node;
}
