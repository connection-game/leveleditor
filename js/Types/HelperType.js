"use strict";

const HelperType = generateDefaultType("helper", { "block": "helper", "text": "" });
HelperType.prototype.customInitCellDom = generateTextDiv("?");
HelperType.prototype.matchData = (d) => d.block == "helper";
HelperType.prototype.initTypeSettingsDom = function() {
    this.typeSettingsDom = document.createElement("div");
    this.typeSettingsDom.classList.add("typeSettings");

    let title = document.createElement("h3");
    title.innerText = "helper";
    title.classList.add("title");
    this.typeSettingsDom.append(title);

    let inputContainer = document.createElement("div");
    inputContainer.classList.add("objectIdContainer");

    let input = document.createElement("textarea");
    input.placeholder = "description";

    let saveButton = document.createElement("button");
    saveButton.innerHTML = "save";
    saveButton.addEventListener("click", () => {
        this.data.text = input.value;
        this.cellDom.title = input.value;
    });

    inputContainer.append(input, saveButton);
    this.typeSettingsDom.append(inputContainer);
}

HelperType.prototype.generateCell = function (data) {
    let node = this.cellDom.cloneNode(true);
    node.title = data.text;
    return node;
}
