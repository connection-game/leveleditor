"use strict";

let GeneratorType = generateDefaultType("generator", { "block": "generator", "direction": "top" });
GeneratorType.prototype.customInitCellDom = generateTextDiv("G");
GeneratorType.prototype.matchData = (d) => d.block == "generator";
GeneratorType.prototype.initTypeSettingsDom = function() {
    this.typeSettingsDom = document.createElement("div");
    this.typeSettingsDom.classList.add("typeSettings");

    let title = document.createElement("h3");
    title.innerText = "generator";
    title.classList.add("title");
    this.typeSettingsDom.append(title);

    let inputContainer = document.createElement("div");
    inputContainer.classList.add("inputContainer");

    let label = document.createElement("span");
    label.innerHTML = "direction";

    let directionsSelect = document.createElement("select");

    let directions = ["top", "topRight", "right", "rightDown", "down", "downLeft", "left", "leftTop"];
    directions.forEach(dir => {
        let option = document.createElement("option");
        option.text = dir;
        option.value = dir;
        directionsSelect.add(option);
    });

    directionsSelect.addEventListener("change", () => {
        let dir = directions[directionsSelect.selectedIndex];
        this.arrowDom.setAttribute("data-direction", dir);
        this.data.direction = dir;
    });

    inputContainer.append(label, directionsSelect);
    this.typeSettingsDom.append(inputContainer);
};

GeneratorType.prototype.customInitCellDom = function() {
    let intDiv = document.createElement("div");
    intDiv.textContent = "G";
    intDiv.classList.add("textDiv");

    this.arrowDom = document.createElement("div");
    this.arrowDom.setAttribute("data-direction", "top");
    this.arrowDom.classList.add("arrow");

    this.cellDom.append(intDiv, this.arrowDom);
    this.cellDom.classList.add("textDivContainer");
}

GeneratorType.prototype.generateCell = function (data) {
    let node = this.cellDom.cloneNode(true);
    node.querySelector(".arrow").setAttribute("data-direction", data.direction);
    return node;
}
