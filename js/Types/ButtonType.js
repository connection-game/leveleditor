"use strict";

const ButtonType = generateDefaultType("button", { "block": "button" });
ButtonType.prototype.customInitCellDom = generateTextDiv("BT");
ButtonType.prototype.matchData = (d) => d.block == "button";
ButtonType.prototype.initTypeSettingsDom = function() {
    this.typeSettingsDom = document.createElement("div");
    this.typeSettingsDom.classList.add("typeSettings");

    let title = document.createElement("h3");
    title.innerText = "button";
    title.classList.add("title");
    this.typeSettingsDom.append(title);

    let inputContainer = document.createElement("div");
    inputContainer.classList.add("objectIdContainer");

    let label = document.createElement("span");
    label.innerHTML = "object id";
    label.classList.add("objectIdContainer");

    let input = document.createElement("input");
    input.maxLength = 4;

    let statusBar = new StatusBar()
    let saveButton = document.createElement("button");
    saveButton.innerHTML = "save";
    saveButton.addEventListener("click", () => {
        if(input.value.includes("\"")) {
            statusBar.flashError("id contains invalid characters");
        } else {
            this.data.objectId = input.value;
            this.idDom.textContent = input.value;
        }
    });

    inputContainer.append(label, input, saveButton);
    this.typeSettingsDom.append(inputContainer);
};

ButtonType.prototype.customInitCellDom = function() {
    let intDiv = document.createElement("div");
    intDiv.textContent = "BT";
    intDiv.classList.add("textDiv");

    this.idDom = document.createElement("span");
    this.idDom.classList.add("objectId");

    this.cellDom.append(intDiv, this.idDom);
    this.cellDom.classList.add("textDivContainer");
}

ButtonType.prototype.generateCell = function (data) {
    let node = this.cellDom.cloneNode(true);
    node.querySelector(".objectId").textContent = data.objectId;
    return node;
}
