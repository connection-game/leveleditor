"use strict";

class ConnectorType extends Type {
    constructor() {
        super();
        this.data = { "block": "connector" };
        this.group = "block";
        this.initCellDom();
        this.initTypeSettingsDom();
        this.onChange = () => {};
    }

    static getGroup() {
        return group;
    }

    matchData(d) {
        return d.block == "connector";
    }

    constraint(levelData, x, y) {
        if(levelData[y][x].wall !== null) {
            return [true];
        } else {
            return [false, "block cannot be placed on an empty field"];
        }
    }

    levelDataConverter(data) {
        if(data.wall == null) {
            return null;
        }
    }

    initCellDom() {
        this.cellDom = document.createElement("div");
        this.cellDom.classList.add("cell");
        this.cellDom.classList.add("connectorType");
        this.cellDom.classList.add(this.group + "Group");

        let intDiv = document.createElement("div");
        intDiv.textContent = "C";
        intDiv.classList.add("textDiv");
        this.cellDom.append(intDiv);
        this.cellDom.classList.add("textDivContainer");
    }

    initTypeSettingsDom() {
        this.typeSettingsDom = document.createElement("div");
        this.typeSettingsDom.classList.add("typeSettings");

        let title = document.createElement("h3");
        title.innerText = "connector";
        title.classList.add("title");
        this.typeSettingsDom.append(title);

        let input = document.createElement("div");

        let moveableCheckbox = document.createElement("input");
        moveableCheckbox.type = "checkbox";
        moveableCheckbox.addEventListener("change", () => {
            if(moveableCheckbox.checked) {
                this.cellDom.classList.add("moveable"); //yes, i know about toggle
            } else {
                this.cellDom.classList.remove("moveable");
            }

            this.data.moveable = moveableCheckbox.checked ? true : undefined;
            this.onChange(this.data);
        });

        let label = document.createElement("span");
        label.innerHTML = "moveable";

        input.append(moveableCheckbox, label);
        this.typeSettingsDom.append(input);
    }

    generateCell(data) {
        let cell = this.cellDom.cloneNode(true);
        if(data.moveable) {
            cell.classList.add("moveable");
        }

        return cell;
    }

    restore(data) { 
        this.data.moveable = data.moveable;
    }

    setOnChange(fun) {
        this.onChange = fun;
    }
}

levelTypes.push(ConnectorType);
