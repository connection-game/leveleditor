"use strict";

class WallType extends Type {
    constructor() {
        super();
        this.data = {};
        this.group = "wall";
        this.initCellDom();
        this.initTypeSettingsDom();
        this.onChange = () => {};
    }

    initCellDom() {
        this.cellDom = document.createElement("div");
        this.cellDom.classList.add("wallType");
        this.cellDom.classList.add("cell");
        this.cellDom.classList.add(this.group + "Group");
    }

    static getGroup() {
        return "wall";
    }

    matchData(d) {
        if(d != null) {
            return ["top", "left", "right", "bottom"].some(side => d[side] == "glass" || d[side] == "wall" || d[side] == "fog");
        } else {
            return d;
        }
    }

    constraint() {
        return [true];
    }

    initTypeSettingsDom() {
        this.typeSettingsDom = document.createElement("div");
        this.typeSettingsDom.classList.add("typeSettings");

        let title = document.createElement("h3");
        title.innerText = "wall";
        title.classList.add("title");
        this.typeSettingsDom.append(title);

        ["top", "left", "right", "bottom"].forEach(side => {
            let input = document.createElement("div");

            let radioContiner = document.createElement("div");
            radioContiner.classList.add("radioContainer");

            ["wall", "glass", "fog", "none"].forEach(wallType => {
                let radioLabel = document.createElement("span");
                radioLabel.innerHTML += wallType;
                radioLabel.classList.add("radioContinerLabel");
                    
                let radio = document.createElement("input");
                radio.type = "radio";
                radio.name = side;

                radio.addEventListener("change", () => {
                    let sideStr = capitalize(side);
                    if(wallType == "wall") { //switch? yeah, make break not required first
                        this.cellDom.classList.remove("glassType" + sideStr);
                        this.cellDom.classList.remove("fogType" + sideStr);
                        this.cellDom.classList.add("wallType" + sideStr);
                    } else if(wallType == "glass") {
                        this.cellDom.classList.remove("wallType" + sideStr);
                        this.cellDom.classList.remove("fogType" + sideStr);
                        this.cellDom.classList.add("glassType" + sideStr);
                    } else if(wallType == "fog") {
                        this.cellDom.classList.remove("wallType" + sideStr);
                        this.cellDom.classList.remove("glassType" + sideStr);
                        this.cellDom.classList.add("fogType" + sideStr);
                    } else {
                        this.cellDom.classList.remove("wallType" + sideStr);
                        this.cellDom.classList.remove("glassType" + sideStr);
                        this.cellDom.classList.remove("fogType" + sideStr);
                    }

                    this.data[side] = wallType == "none" ? undefined : wallType;
                    this.onChange(this.data);
                });

                radioContiner.append(radio, radioLabel);
            });

            let label = document.createElement("span");
            label.innerHTML = side;
            label.classList.add("radioContainerLabel");

            input.append(label, radioContiner);
            this.typeSettingsDom.append(input);
        });
    }

    generateCell(data) {
        let cell = this.cellDom.cloneNode(true);
        ["top", "left", "right", "bottom"].forEach(side => {
            if(data[side] == "wall") {
                cell.classList.add("wallType" + capitalize(side));
                cell.classList.remove("glassType" + capitalize(side));
                cell.classList.remove("fogType" + capitalize(side));
            } else if(data[side] == "glass") {
                cell.classList.add("glassType" + capitalize(side));
                cell.classList.remove("wallType" + capitalize(side));
                cell.classList.remove("fogType" + capitalize(side));
            } else if(data[side] == "fog") {
                cell.classList.add("fogType" + capitalize(side));
                cell.classList.remove("wallType" + capitalize(side));
                cell.classList.remove("glassType" + capitalize(side));
            } else {
                cell.classList.remove("wallType" + capitalize(side));
                cell.classList.remove("glassType" + capitalize(side));
                cell.classList.remove("fogType" + capitalize(side));
            }
        });

        return cell;
    }

    setOnChange(fun) {
        this.onChange = fun;
    }
}

levelTypes.push(WallType);
