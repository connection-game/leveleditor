"use strict";

const EmptyType = generateDefaultType("empty", { wall: null }, null);
EmptyType.prototype.constraint = () => [true];
EmptyType.prototype.converter = (d) => d.wall === null ? null : d;
EmptyType.prototype.deconverter = (d) => d === null ? { wall: null } : d;
EmptyType.prototype.matchData = (d) => d == null; //it just keeps getting worse and worse

const EmptyBlockType = generateDefaultType("emptyBlock", {}, "block");
EmptyBlockType.prototype.constraint = () => [true];
EmptyBlockType.prototype.matchData = (d) => d == {};

const ClearWallsType = generateDefaultType("clearWalls", {}, "wall");
ClearWallsType.prototype.constraint = () => [true];
ClearWallsType.prototype.matchData = (d) => d == {};

const BoxType = generateDefaultType("box", { "block": "box" });
BoxType.prototype.customInitCellDom = generateTextDiv("B");
BoxType.prototype.matchData = (d) => d.block == "box";

const Player1Type = generateDefaultType("player1", { "block": "player1" });
Player1Type.prototype.customInitCellDom = generateTextDiv("P1");
Player1Type.prototype.matchData = (d) => d.block == "player1";
Player1Type.prototype.constraint = singletonConstraint("player1");

const Player2Type = generateDefaultType("player2", { "block": "player2" });
Player2Type.prototype.customInitCellDom = generateTextDiv("P2");
Player2Type.prototype.matchData = (d) => d.block == "player2";
Player2Type.prototype.constraint = singletonConstraint("player2");
