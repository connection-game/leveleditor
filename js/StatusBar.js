"use strict";

class StatusBar {
    constructor(dom=document.getElementById("status")) {
        this.dom = dom;
        this.messageTimeout = undefined;
        this.regularText = "";
        this.delay = 2000;
    }

    flashMessage(message, cssClass) {
        this.resetState();

        this.dom.innerHTML = message;
        this.dom.classList.add(cssClass);

        this.messageTimeout = setTimeout(() => {
            this.dom.innerHTML = this.regularText;
            this.dom.classList.remove(cssClass);
        }, this.delay);
    }

    flashInfo(message) {
        this.flashMessage(message, "info");
    }

    flashError(message) {
        this.flashMessage(message, "error");
    }

    setText(message) {
        this.resetState();
        this.regularText = message;
        this.dom.innerHTML = message;
    }

    resetState() {
        if(this.messageTimeout !== undefined) 
            clearTimeout(this.messageTimeout);

        this.dom.classList.remove("error");
        this.dom.classList.remove("info");
        this.dom.innerHTML = this.regularText;
    }
}
