"use strict";

class LevelEditor {
    constructor(statusBar) {
        this.levelData = new LevelData();
        this.levelView = new LevelView();
        this.elementPicker = new ElementPicker();
        this.types = [];
        this.statusBar = statusBar;

        this.levelView.setOnCellClicked((x, y, cell) => {
            let res = this.levelData.setCell(x, y);
            //console.table(this.levelData.data);

            if(!res[0]) {
                statusBar.flashError(res[1]);
            }

            return res[0];
        });

        this.elementPicker.setOnChange((type) => {
            this.levelData.setType(type);
            this.levelView.setType(type);
        });
    }

    setLevelSize(w, h) {
        this.levelData.generate(w, h);
        this.levelView.generate(w, h);
    }

    addType(type) {
        this.types.push(type);
        this.elementPicker.addType(type);
        this.levelData.addType(type);
        this.levelView.addType(type);
    }

    addTypes(types) {
        types.forEach(t => this.addType(new t));
    }

    setType(type) {
        let typeInstance = this.types.find(t => t instanceof type);
        if(typeInstance != undefined) {
            this.elementPicker.setCurrentType(typeInstance);
            this.levelView.setType(typeInstance);
            this.levelData.setType(typeInstance);
        } else {
            console.log("type not found", type);
        }
    }

    getData() {
        return this.levelData.getData();
    }

    loadLevel(level) {
        this.levelData.loadData(level.data);
        this.levelView.loadData(this.levelData.data); //you absoulute moron
    }
}
